*** Settings ***
Resource   ../../Resources/main.robot

*** Variables ***
&{home}
#-------------------------------------------------- CABECALHO ---------------------------------------------------#
...    btn_menu=//a[@tabindex='51']
...    field_pesquisa=txtPesquisaRapida
...    btn_lupa=spnInfraUnidade
...    btn_controle_processo_cabecalho=(//img[@src='svg/controle_processos_barra.svg'])[1]
...    title_controle_processo=divInfraBarraLocalizacao
...    btn_novidades=(//img[@src='svg/novidades.svg'])[1]
...    btn_usuario_topo=(//img[@src='/infra_css/svg/usuario_topo.svg?1.615.4'])[1]
...    btn_configuracao_topo=(//img[@src='/infra_css/svg/configuracao.svg?1.615.4'])[1]
...    btn_sair=(//img[@src='/infra_css/svg/sair.svg?1.615.4'])[1]

#--------------------------------------------------- ICONES ----------------------------------------------------#
...    icon_enviar_processo=//img[@title='Enviar Processo']
...    icon_atualizar_andamento=//img[@title='Atualizar Andamento']
...    icon_atribuicao_processos=//img[@title='Atribuição de Processos']
...    icon_incluir_bloco=//img[@title='Incluir em Bloco']
...    icon_sobrestar_processo=//img[@title='Sobrestar Processo']
...    icon_concluir_processo_nessa_unidade=//img[@title='Concluir Processo nesta Unidade']
...    icon_anotacoes=//img[@title='Anotações']
...    icon_acompanhamento_especial=//img[@title='Acompanhamento Especial']
...    icon_incluir=//img[contains(@alt,'Incluir Documento')]
...    icon_adicionar_marcador=//img[contains(@alt,'Adicionar Marcador')]
...    icon_remover_marcador=//img[contains(@alt,'Remover Marcador')]
...    icon_controle_prazos=//img[contains(@alt,'Controle de Prazos')]
...    link_visualizacao_detalhada=//a[contains(.,'Visualização detalhada')]
...    link_ver_processo_atribuidos=//a[contains(.,'Ver processos atribuídos a mim')]
...    link_ver_por_marcadores=//a[contains(.,'Ver por marcadores')]
...    link_ver_por_tipo=//a[contains(.,'Ver por tipo de processo')]

#--------------------------------------------------- MENUS ----------------------------------------------------#
...    btn_iniciar_processo=//span[contains(.,'Iniciar Processo')]

*** Keywords ***
Validar Home
#-------------------------------------------------- CABECALHO ---------------------------------------------------#
    Wait Until Element Is Visible    ${home.title_controle_processo}
    Element Should Be Visible        ${home.btn_menu}
    Element Should Be Visible        ${home.btn_lupa}
    Element Should Be Visible        ${home.field_pesquisa}
    Element Should Be Visible        ${home.btn_controle_processo_cabecalho}
    Element Should Be Visible        ${home.btn_novidades}
    Element Should Be Visible        ${home.btn_usuario_topo}
    Element Should Be Visible        ${home.btn_configuracao_topo}
    Element Should Be Visible        ${home.btn_sair}
#--------------------------------------------------- ICONES ----------------------------------------------------#
    Element Should Be Visible        ${home.icon_enviar_processo}
    Element Should Be Visible        ${home.icon_atualizar_andamento}
    Element Should Be Visible        ${home.icon_atribuicao_processos}
    Element Should Be Visible        ${home.icon_incluir_bloco}
    Element Should Be Visible        ${home.icon_sobrestar_processo}
    Element Should Be Visible        ${home.icon_concluir_processo_nessa_unidade}
    Element Should Be Visible        ${home.icon_anotacoes}
    Element Should Be Visible        ${home.icon_acompanhamento_especial}
    Element Should Be Visible        ${home.icon_incluir}
    Element Should Be Visible        ${home.icon_adicionar_marcador}
    Element Should Be Visible        ${home.icon_concluir_processo_nessa_unidade}
    Element Should Be Visible        ${home.link_visualizacao_detalhada}
    Element Should Be Visible        ${home.link_ver_processo_atribuidos}
    Element Should Be Visible        ${home.link_ver_por_marcadores}
    Element Should Be Visible        ${home.link_ver_por_tipo}
#--------------------------------------------------- MENUS ----------------------------------------------------#
    Element Should Be Visible        ${home.btn_iniciar_processo}

Clicar em Iniciar Processo
    Click Element    ${home.btn_iniciar_processo}

