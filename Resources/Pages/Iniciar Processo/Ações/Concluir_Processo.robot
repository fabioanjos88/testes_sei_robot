*** Settings ***
Resource   ../../../../Resources/main.robot


*** Variables ***
&{concluir_processo}
...    field_info_processo_concluido=id:divArvoreInformacao

*** Keywords ***
Confirmar conclusão do processo
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_ifr_arvore}
    Wait Until Element Is Visible           ${tela_processo.text_numero_processo_criado}    7s
    Click Element                           ${tela_processo.text_numero_processo_criado}
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${concluir_processo.field_info_processo_concluido}
    ${TEXTO}    Get Text                    ${concluir_processo.field_info_processo_concluido}
    Log To Console                          ${TEXTO}
    Should Be Equal                         ${TEXTO}    Processo não possui andamentos abertos.
