*** Settings ***
Resource   ../../../../Resources/main.robot
Library    FakerLibrary

*** Variables ***
&{incluir_documento}
...    field_texto_inicial=id:fldTextoInicial
...    field_descricao=id:txtDescricao
...    field_interessados=id:txtInteressado
...    field_destinatarios=id:txtDestinatario
...    field_destinatario_inserido=id:selDestinatarios
...    radioButton_documento_modelo=id:divOptProtocoloDocumentoTextoBase
...    radioButton_texto_padrao=id:divOptTextoPadrao
...    radioButton_nenhum=id:lblNenhum

*** Keywords ***
Selecionar tipo de documento "${REGISTRAR_DOCUMENTO}" para ser incluído
    Wait Until Element Is Visible           ${tela_processo.input_escolha_tipo_documento}
    Click Element                           ${tela_processo.btn_mais_documentos}
    Input Text                              ${tela_processo.input_escolha_tipo_documento}    ${REGISTRAR_DOCUMENTO}
    (KEYWORD) ARROW_DOWN + ENTER

Preencher dados da capa do documento incluido
    Wait Until Element Is Visible           ${capa_processo.field_texto_inicial_despacho}
    Input Text                              ${incluir_documento.field_descricao}          Teste automatizado - ${DATA}
    ${RANDOM_NUMBER_INCLUIR}                FakerLibrary.Random int    min=10    max=100
    Input Text                              ${incluir_documento.field_interessados}       Interessado ${DATA} (${RANDOM_NUMBER_INCLUIR})
    (KEYWORD) ENTER
    (KEYWORD) ACCEPT_ALERT
    Input Text                              ${incluir_documento.field_destinatarios}      Destinatario ${DATA} (${RANDOM_NUMBER_INCLUIR})
    (KEYWORD) ENTER
    (KEYWORD) ACCEPT_ALERT
    Input Text                              ${capa_processo.field_observacoes}            Observação ${DATA}
    Click Element                           ${capa_processo.radio_nivel_publico}
    Click Element                           ${capa_processo.btn_salvar}
