*** Settings ***
Resource   ../../../../Resources/main.robot

*** Variables ***
&{ciencia}
...    title_ciencias=xpath://div[contains(.,'Ciências')]
...    field_descricao_ciencia_no_processo=xpath://td[contains(.,'Ciência no processo')]
...    cabecalho_data_hora=xpath://th[contains(.,'Data/Hora')]
...    cabecalho_unidade=xpath://th[contains(.,'Unidade')]
...    cabecalho_usuario=xpath://th[contains(.,'Usuário')]
...    cabecalho_descricao=xpath://th[contains(.,'Descrição')]
...    field_usuario=xpath://td[contains(.,'fsanjos')]
...    icon_visualizar_ciencia=xpath://img[@title='Visualizar Ciências']
...    icon_menu_visualizar_ciencia=iconC


*** Keywords ***
Validacao Tela Ciência  
    Wait Until Element Is Visible           ${ciencia.field_descricao_ciencia_no_processo}
    Element Should Be Visible               ${ciencia.cabecalho_data_hora}
    Element Should Be Visible               ${ciencia.cabecalho_unidade}
    Element Should Be Visible               ${ciencia.cabecalho_usuario}
    Element Should Be Visible               ${ciencia.field_descricao_ciencia_no_processo}
    ${DESCRICAO_STATUS_ATUAL_CIENCIA}       Get Text                             ${ciencia.field_descricao_ciencia_no_processo}
    Should Be Equal                         ${DESCRICAO_STATUS_ATUAL_CIENCIA}    Ciência no processo
    ${USUARIO_ATUAL_CIENCIA}                Get Text                             ${ciencia.field_usuario}
    Should Be Equal                         ${USUARIO_ATUAL_CIENCIA}             ${USUARIO_FABIO}
    