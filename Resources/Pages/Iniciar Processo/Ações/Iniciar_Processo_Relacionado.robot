*** Settings ***
Resource   ../../../../Resources/main.robot

*** Variables ***
&{processo_relacionado}
...    menu_processos_relacionado=id:divRelacionados
...    text_tipo_processo=id:divInfraBarraLocalizacao
...    btn_exibir_todos_tipos=id:ancExibirTiposProcedimento

*** Keywords ***
Selecionar processo relacionado do tipo "${TIPO_PROCESSO}"
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${processo_relacionado.text_tipo_processo}
    Click Element                           ${processo_relacionado.btn_exibir_todos_tipos}
    Input Text                              ${iniciar_processo.field_escolha_tipo_processo}   ${TIPO_PROCESSO}
    (KEYWORD) ARROW_DOWN + ENTER

Preencher dados da capa do processo relacionado e salvar
    Input Text                              ${capa_processo.field_descricao}       Teste automatizado - ${DATA}
    Input Text                              ${capa_processo.field_interessados}    Interessado ${DATA}
    (KEYWORD) ENTER
    (KEYWORD) ACCEPT_ALERT
    Input Text                              ${capa_processo.field_observacoes}     Observação ${DATA}
    Click Element                           ${capa_processo.radio_nivel_publico}
    Click Element                           ${capa_processo.btn_salvar}

Confirmar Processo Relacionado
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_ifr_arvore}
    Wait Until Element Is Visible           ${processo_relacionado.menu_processos_relacionado}
    ${PROCESSO_RELACIONADO}                 Get Text     ${processo_relacionado.menu_processos_relacionado}
    Should Be Equal                         ${PROCESSO_RELACIONADO}               Processos Relacionados: