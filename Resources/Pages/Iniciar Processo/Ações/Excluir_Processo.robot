*** Settings ***
Resource   ../../../../Resources/main.robot
Library    FakerLibrary

*** Variables ***
&{excluir}

*** Keywords ***
Confirmar Exclusão do Processo
    Wait Until Element Is Visible    ${home.field_pesquisa}
    Input Text                       ${home.field_pesquisa}    ${NUMERO_PROCESSO_CRIADO}
    Click Element                    ${home.btn_lupa}
    Wait Until Element Is Visible    ${tela_pesquisa.field_resultado_pesquisa}
    ${TEXTTO}            Get Text    ${tela_pesquisa.field_resultado_pesquisa}
    Element Should Contain           ${tela_pesquisa.field_resultado_pesquisa}    Nenhum resultado encontrado.
    