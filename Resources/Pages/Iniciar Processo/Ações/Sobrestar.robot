*** Settings ***
Resource   ../../../../Resources/main.robot

*** Variables ***
&{sobrestar}
...    title_sobrestamento=id:divInfraBarraLocalizacao
...    field_info_processo_sobrestado=id:selProcedimentos
...    field_motivo=id:txaMotivo
...    radioButton_somente_sobrestar=id:divOptSomenteSobrestar
...    radioButton_sobrestar_vincular=id:divOptSobrestarVincular
...    btn_salvar=id:sbmSalvar
...    field_confirmacao_sobrestamento=id:divArvoreInformacao
...    field_processo_vincular=id:txtProcedimentoDestino
...    btn_pesquisar_vincular=id:btnPesquisar
...    field_tipo_vincular=id:txtIdentificacaoProcedimentoDestino
...    icon_remover_sobrestamento=xpath://img[contains(@alt,'Remover Sobrestamento do Processo')]
...    icon_sobrestar=xpath://img[contains(@alt,'Sobrestar Processo')]

*** Keywords ***
Preencher sobrestar vinculado
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_ifr_arvore}
    ${NUMERO_PROCESSO}          Get Text    ${tela_processo.text_numero_processo_criado}
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${sobrestar.title_sobrestamento}
    Element Should Contain                  ${sobrestar.field_info_processo_sobrestado}    ${NUMERO_PROCESSO}
    Click Element                           ${sobrestar.radioButton_sobrestar_vincular}
    Input Text                              ${sobrestar.field_processo_vincular}    000.00001094/2023-39
    Click Element                           ${sobrestar.btn_pesquisar_vincular}
    # Wait Until Page Contains                Assinatura de Convênios
    Sleep    2s
    Input Text                              ${sobrestar.field_motivo}    (SOBRESTADO VINCULADO) Teste automatizado ${DATA}
    Click Element                           ${sobrestar.btn_salvar}
    Wait Until Element Is Visible           ${sobrestar.field_confirmacao_sobrestamento}
    Element Should Contain                  ${sobrestar.field_confirmacao_sobrestamento}    Processo sobrestado na unidade
    Wait Until Element Is Visible           ${sobrestar.icon_remover_sobrestamento}
    Click Element                           ${sobrestar.icon_remover_sobrestamento}
    (KEYWORD) ACCEPT_ALERT

Preencher somemte sobrestar
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_ifr_arvore}
    ${NUMERO_PROCESSO}          Get Text    ${tela_processo.text_numero_processo_criado}
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${sobrestar.title_sobrestamento}
    Element Should Contain                  ${sobrestar.field_info_processo_sobrestado}    ${NUMERO_PROCESSO}
    Click Element                           ${sobrestar.radioButton_somente_sobrestar}
    Input Text                              ${sobrestar.field_motivo}    (SOMENTE SOBRESTAR) Teste automatizado ${DATA}
    Click Element                           ${sobrestar.btn_salvar}
    Wait Until Element Is Visible           ${sobrestar.field_confirmacao_sobrestamento}
    Element Should Contain                  ${sobrestar.field_confirmacao_sobrestamento}    Processo sobrestado na unidade



    