*** Settings ***
Resource   ../../../../Resources/main.robot
Library    FakerLibrary

*** Variables ***
&{enviar}
...    title_titulo_enviar_processo=id:divInfraBarraLocalizacao
...    field_numero_processo_criado=id:header
...    field_numero_processo_relacionado=id:selProcedimentos
...    select_orgao=id:selOrgao
...    select_unidade=id:txtUnidade
...    checkBox_manter_aberto_na_unidade=id:lblSinManterAberto
...    unidade_padrao=TESTE_1_1 - Unidade de Testes 1.1
...    radioButton_prazo_em_dias=id:lblDias
...    field_dias_uteis=id:txtDias
...    btn_enviar=id:sbmEnviar
...    field_arvore_informacao=id:divArvoreInformacao

*** Keywords ***

Preencher dados da capa do documento a ser enviado e enviar
    Select Frame                     ${tela_processo.iframe_ifr_arvore}
    ${numero_processo_menu}          Get Text    ${enviar.field_numero_processo_criado}
    Log To Console                   ${numero_processo_menu}
    Unselect Frame
    Select Frame                     ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible    ${enviar.title_titulo_enviar_processo}
    Element Should Contain           ${enviar.field_numero_processo_relacionado}    ${numero_processo_menu}
    Select From List By Label        ${enviar.select_orgao}               GESP
    Input Text                       ${enviar.select_unidade}             TESTE_1_1    
    Wait Until Page Contains         ${enviar.unidade_padrao} 
    (KEYWORD) ARROW_DOWN + ENTER
    Click Element                    ${enviar.checkBox_manter_aberto_na_unidade}
    Click Element                    ${enviar.radioButton_prazo_em_dias} 
    Input Text                       ${enviar.field_dias_uteis}           100
    Click Element                    ${enviar.btn_enviar}
    Wait Until Element Is Visible    ${enviar.field_arvore_informacao}
    ${TEXTO}    Get Text             ${enviar.field_arvore_informacao}
    Log To Console                   ${TEXTO}
    Element Should Contain           ${enviar.field_arvore_informacao}    Processo aberto nas unidades:    TESTE_1_1    TESTE
