*** Settings ***
Resource   ../../../../Resources/main.robot


*** Variables ***
&{consultar_alterar}
...    filed_tipo_processo=id:selTipoProcedimento
...    btn_salvar=id:btnSalvar
...    field_descricao=id:txtDescricao
...    field_interessado=id:txtInteressadoProcedimento

*** Keywords ***
Alterar o tipo do processo para "${PROCESSO_ALTERADO}"
    Select Frame                     ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible    ${consultar_alterar.filed_tipo_processo}
    Select From List By Label        ${consultar_alterar.filed_tipo_processo}    ${PROCESSO_ALTERADO}
    Input Text                       ${consultar_alterar.field_descricao}        Alterado - ${DATA}
    Input Text                       ${consultar_alterar.field_interessado}      Alterado - ${DATA}
    (KEYWORD) ARROW_DOWN + ENTER
    (KEYWORD) ACCEPT_ALERT
    Click Element                    ${consultar_alterar.btn_salvar}