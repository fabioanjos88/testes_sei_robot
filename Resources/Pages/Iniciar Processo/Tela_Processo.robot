*** Settings ***
Resource   ../../../Resources/main.robot

*** Variables ***
&{tela_processo}
#---------------------------------------------- IFRAMES ----------------------------------------------#
...    iframe_visualizacao=id:ifrVisualizacao
...    iframe_arvore_acoes=id:divArvoreAcoes
...    iframe_ifr_arvore=id:ifrArvore
...    iframe_popup_assinar=name:modal-frame
    
#---------------------------------------------- ICONES ----------------------------------------------=#
...    icon_inciar_processo_relacionado=xpath://img[@title='Iniciar Processo Relacionado']
...    icon_incluir_documento=xpath://img[contains(@alt,'Incluir Documento')]
...    icon_assinar=xpath://img[contains(@alt,'Assinar Documento')]
...    icon_ciencia=xpath://img[contains(@alt,'Ciência')]
...    icon_consultar_alterar=xpath://img[contains(@alt,'Consultar/Alterar Processo')]
...    icon_enviar=xpath://img[contains(@alt,'Enviar Processo')]
...    icon_concluir_processo=xpath://img[contains(@alt,'Concluir Processo')]
...    icon_sobrestar=//img[contains(@alt,'Sobrestar Processo')]
...    icon_excluir_processo=//img[contains(@alt,'Excluir')]


#---------------------------------------------- OUTROS ----------------------------------------------=#
...    btn_salvar=xpath://button[@tabindex='32701'][contains(.,'Salvar')]
...    btn_popup_salvar=id:cke_23
...    btn_mais_documentos=xpath://img[contains(@title,'Exibir todos os tipos')]
...    btn_popup_assinar=xpath://button[@tabindex='451'][contains(.,'Assinar')]
...    input_escolha_tipo_documento=xpath://input[contains(@type,'text')]
...    field_senha=xpath://input[contains(@id,'pwdSenha')]
...    text_numero_processo_criado=id:header

*** Keywords ***
Validar Página de Tela de Processo
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.iframe_arvore_acoes}
    Element Should Be Visible               ${tela_processo.icon_ciencia}
    Element Should Be Visible               ${tela_processo.icon_inciar_processo_relacionado}

Coletar número do Processo Criado
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_ifr_arvore}
    Wait Until Element Is Visible           ${tela_processo.text_numero_processo_criado}    7s
    ${NUMERO_PROCESSO_CRIADO}               Get Text    ${tela_processo.text_numero_processo_criado}
    Log                                     ${NUMERO_PROCESSO_CRIADO}
    Set Global Variable                     ${NUMERO_PROCESSO_CRIADO}  
    Log To Console                          ${NUMERO_PROCESSO_CRIADO}
    Unselect Frame

Assinar
    Wait Until Element Is Visible           ${tela_processo.icon_assinar}    1m
    Click Element                           ${tela_processo.icon_assinar}
    Unselect Frame
    Select Frame                            ${tela_processo.iframe_popup_assinar}
    Input Text                              ${tela_processo.field_senha}    ${SENHA_FABIO}
    Click Element                           ${tela_processo.btn_popup_assinar}

Clicar em Incluir Documento
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.icon_incluir_documento}
    Click Element                           ${tela_processo.icon_incluir_documento}

Clicar em Ciencia
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.icon_ciencia}
    Click Element                           ${tela_processo.icon_ciencia}

Clicar em Iniciar Processo Relacionado
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.icon_inciar_processo_relacionado}
    Click Element                           ${tela_processo.icon_inciar_processo_relacionado}
    Unselect Frame

Clicar em Consultar/Alterar Processo
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.icon_consultar_alterar}
    Click Element                           ${tela_processo.icon_consultar_alterar}
    Unselect Frame

Clicar em Enviar
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.icon_enviar}
    Click Element                           ${tela_processo.icon_enviar}
    Unselect Frame

Clicar em Concluir Processo
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.icon_concluir_processo}
    Click Element                           ${tela_processo.icon_concluir_processo}
    Unselect Frame

Clicar em Sobrestar
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.icon_sobrestar}
    Click Element                           ${tela_processo.icon_sobrestar}
    Unselect Frame

Clicar em Excluir Processo
    Select Frame                            ${tela_processo.iframe_visualizacao}
    Wait Until Element Is Visible           ${tela_processo.icon_excluir_processo}
    Click Element                           ${tela_processo.icon_excluir_processo}
    (KEYWORD) ACCEPT_ALERT
    Unselect Frame
