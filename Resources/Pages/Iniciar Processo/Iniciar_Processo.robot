*** Settings ***
Resource   ../../../Resources/main.robot

*** Variables ***
&{iniciar_processo}
...    field_escolha_tipo_processo=id:txtFiltro
...    btn_exibir_todos_tipos=id:ancExibirTiposProcedimento
...    text_tipo_processo=id:divInfraBarraLocalizacao


*** Keywords ***
Selecionar processo do tipo "${TIPO_PROCESSO}"
    Wait Until Element Is Visible    ${iniciar_processo.text_tipo_processo}
    Click Element                    ${iniciar_processo.btn_exibir_todos_tipos}
    Input Text                       ${iniciar_processo.field_escolha_tipo_processo}   ${TIPO_PROCESSO}
    (KEYWORD) ARROW_DOWN + ENTER
    