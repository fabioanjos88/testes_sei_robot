*** Settings ***
Resource   ../../../Resources/main.robot


*** Variables ***
&{capa_processo}
...    title=id:divInfraBarraLocalizacao
...    radio_nivel_publico=id:divOptPublico
...    radio_nivel_restrito=id:lblRestrito
...    radio_nivel_sigiloso=id:divOptSigiloso
...    field_nivel_acesso=id:fldNivelAcesso    
...    field_descricao=id:txtDescricao
...    field_classificacao_por_assunto=id:txtAssunto
...    field_assunto_selecionado=id:selAssuntos
...    field_interessados=id:txtInteressadoProcedimento
...    field_interessados_selecionado=id:selInteressadosProcedimento
...    field_observacoes=id:txaObservacoes
...    field_texto_inicial_despacho=id:fldTextoInicial
...    dropdown_tipo_processo=id:divTipoProcedimento
...    btn_salvar=id:btnSalvar
...    btn_voltar=name:btnCancelar

*** Keywords ***
Validar Capa Processo
    Wait Until Element Is Visible    ${capa_processo.title}
    Element Should Be Visible        ${capa_processo.radio_nivel_publico}
    Element Should Be Visible        ${capa_processo.field_assunto_selecionado}
    Element Should Be Visible        ${capa_processo.field_interessados}
    Element Should Be Visible        ${capa_processo.radio_nivel_restrito}
    Element Should Be Visible        ${capa_processo.radio_nivel_sigiloso}
    Element Should Be Visible        ${capa_processo.field_descricao}
    Element Should Be Visible        ${capa_processo.field_nivel_acesso}
    Element Should Be Visible        ${capa_processo.field_classificacao_por_assunto}
    Element Should Be Visible        ${capa_processo.btn_salvar}
    Element Should Be Visible        ${capa_processo.btn_voltar}
    Element Should Be Visible        ${capa_processo.dropdown_tipo_processo}
    Element Should Be Visible        ${capa_processo.field_interessados_selecionado}
    Element Should Be Visible        ${capa_processo.field_observacoes}

Preencher dados da capa do processo e salvar
    Input Text                       ${capa_processo.field_descricao}       Teste automatizado - ${DATA}
    Input Text                       ${capa_processo.field_interessados}    Interessado ${DATA} (${RANDOM_NUMBER})
    (KEYWORD) ENTER
    (KEYWORD) ACCEPT_ALERT
    Input Text                       ${capa_processo.field_observacoes}     Observação ${DATA}
    Click Element                    ${capa_processo.radio_nivel_publico}
    Click Element                    ${capa_processo.btn_salvar}

