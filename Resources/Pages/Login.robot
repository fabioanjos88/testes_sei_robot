*** Settings ***
Resource   ../../Resources/main.robot

*** Variables ***
&{login}
...    orgao_padrao=GESP
...    logo_sei_login=id:divLogo
...    field_login=id:txtUsuario
...    field_senha=xpath://input[contains(@placeholder,'Senha')]
...    btn_acessar=id:Acessar
...    dropdown_selecionar_orgao=xpath://select[contains(@name,'selOrgao')]
...    link_autenticacao_em_dois_fatores=xpath://a[contains(@class,'pl-1 linkLogin')]

*** Keywords ***
Fazer login
    Wait Until Element Is Visible    ${login.logo_sei_login}
    Element Should Be Visible        ${login.link_autenticacao_em_dois_fatores}
    Input Text                       ${login.field_login}    ${USUARIO_FABIO}
    Input Text                       ${login.field_senha}    ${SENHA_FABIO}
    Select From List By Label        ${login.dropdown_selecionar_orgao}    ${login.orgao_padrao}
    Click Button                     ${login.btn_acessar}

Fazer login invalido
    Wait Until Element Is Visible    ${login.logo_sei_login}
    Element Should Be Visible        ${login.link_autenticacao_em_dois_fatores}
    Input Text                       ${login.field_login}    ${USUARIO_FABIO}
    Input Password                   ${login.field_senha}    senhaErrada
    Select From List By Label        ${login.dropdown_selecionar_orgao}    ${login.orgao_padrao}
    Click Button                     ${login.btn_acessar}
    Alert Should Be Present

