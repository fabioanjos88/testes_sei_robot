*** Settings ***
Resource   ../../Resources/main.robot
Library    FakerLibrary



*** Keywords ***
Abrir o navegador
    # Create WebDriver    Chrome      executable_path=C:/WebDrivers/chromedriver
    # Set Window Size                  1400       800
    # Go To                            url=${URL_SEI}

#---------------------------------------------------- HEADLESS --------------------------------------------------------#
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${options}    add_argument    --headless
    Create WebDriver    Chrome    chrome_options=${options}    executable_path=C:/WebDrivers/chromedriver
    Set Window Size                  1400       800
    Go To                            url=${URL_SEI}
#----------------------------------------------------------------------------------------------------------------------#

    ${DATA}                          Get Current Date                      result_format=%d/%m/%Y %H:%M
    Set Global Variable              ${DATA}
    ${RANDOM_NUMBER}                 FakerLibrary.Random int    min=10    max=100
    Set Global Variable              ${RANDOM_NUMBER} 

    
Fechar o navegador
    Close Browser   

#-------------------------------------------- DIRECIONAIS --------------------------------------------# 
(KEYWORD) ARROW_DOWN + ENTER
    Press Keys                       None    ARROW_DOWN
    Press Keys                       None    ENTER

(KEYWORD) ENTER
    Press Keys                       None    ENTER

#--------------------------------------------  ALERTAS --------------------------------------------# 
(KEYWORD) ACCEPT_ALERT
    Handle Alert    ACCEPT

(KEYWORD) DISMISS_ALERT
    Handle Alert    action=DISMISS

#-------------------------------------------- SLEEPS --------------------------------------------#
(KEYWORD) WAIT_5s
    Sleep    5s

# Abrir o navegador (SIP)
#     Open Browser                     browser=${BROWSER}   
#     Set Window Size                  1400       800
#     Go To                            url=${URL_SIP}
#     ${DATA}                          Get Current Date                      result_format=%d/%m/%Y %H:%M
#     Set Global Variable              ${DATA}
