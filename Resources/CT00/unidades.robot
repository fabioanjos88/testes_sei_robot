*** Settings ***
Resource    ../main.robot
Library    SeleniumLibrary

*** Variables ***
${ICON_SIP}                    divInfraBarraSistemaPadraoE
${MENU_UNIDADES}               //span[contains(.,'Unidades')]
${MENU_UNIDADES_NOVA}          //span[contains(.,'Nova')]
${DROPDOWN_ORGAO}    id:selOrgao
${FIELD_SIGLA}    id:txtSigla
${FIELD_DESCRICAO}    id:txtDescricao
${SALVAR}    //button[@value='Salvar']

*** Keywords ***
Fazer login SIP
    Wait Until Element Is Visible    ${login.logo_sei_login}
    Input Text                       ${login.field_login}    ${USUARIO_FABIO}
    Input Text                       ${login.field_senha}    ${SENHA_FABIO}
    Select From List By Label        ${login.dropdown_selecionar_orgao}    ${login.orgao_padrao}
    Click Button                     ${login.btn_acessar}
    Wait Until Element Is Visible    ${ICON_SIP}

Acessar meu Unidades
    Wait Until Element Is Visible    ${MENU_UNIDADES}
    Click Element                    ${MENU_UNIDADES}

Clicar em Nova
    Wait Until Element Is Visible    ${MENU_UNIDADES_NOVA}
    Click Element                    ${MENU_UNIDADES_NOVA}
 
Selecionar orgao
    Select From List By Label    ${DROPDOWN_ORGAO}    GESP

Inserir sigla
    [Arguments]    ${sigla_unidade}
    Input Text    ${FIELD_SIGLA}    ${sigla_unidade}

Inserir descricao
    [Arguments]    ${descricao_unidade}
    Input Text    ${FIELD_DESCRICAO}    ${descricao_unidade}

Salvar
    Click Element    ${SALVAR}
    
