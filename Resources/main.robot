*** Settings ***
Library    SeleniumLibrary
Library    DateTime
Library    OperatingSystem

Resource   ../Resources/Shared/Global.robot

Resource   ../Resources/Data/Dados_Gerais.robot

Resource   ../Resources/Pages/login.robot
Resource   ../Resources/Pages/Home.robot
Resource   ../Resources/Pages/Tela_Pesquisa.robot
Resource   ../Resources/Pages/Iniciar Processo/Iniciar_Processo.robot
Resource   ../Resources/Pages/Iniciar Processo/Tela_Processo.robot
Resource   ../Resources/Pages/Iniciar Processo/Capa_Processo.robot

Resource   ../Resources/Pages/Iniciar Processo/Ações/Ciencia.robot
Resource   ../Resources/Pages/Iniciar Processo/Ações/Iniciar_Processo_Relacionado.robot
Resource   ../Resources/Pages/Iniciar Processo/Ações/Incluir_Documento.robot
Resource   ../Resources/Pages/Iniciar Processo/Ações/Consultar_Alterar.robot
Resource   ../Resources/Pages/Iniciar Processo/Ações/Enviar.robot
Resource   ../Resources/Pages/Iniciar Processo/Ações/Concluir_Processo.robot
Resource   ../Resources/Pages/Iniciar Processo/Ações/Sobrestar.robot
Resource   ../Resources/Pages/Iniciar Processo/Ações/Excluir_Processo.robot


Resource   ../Resources/CT00/unidades.robot
