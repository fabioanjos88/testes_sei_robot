*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***
Login valido
    [Tags]    CT01    CT01_01     

    Fazer login
    Validar Home
    
Login invalido
    [Tags]    CT01    CT01_02    

    Fazer login invalido
    