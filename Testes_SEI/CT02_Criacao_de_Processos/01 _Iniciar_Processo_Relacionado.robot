*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***

Iniciar Processo Relacionado
    [Tags]    Regressivo    Iniciar_Processo_Relacionado    CT02    CT02_04


    Fazer login
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Estatuto"
    Preencher dados da capa do processo e salvar
    Clicar em Iniciar Processo Relacionado
    Selecionar processo relacionado do tipo "Expediente de Atendimento"
    Preencher dados da capa do processo relacionado e salvar
    Confirmar Processo Relacionado
    Coletar número do Processo Criado
