*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***

Consultar/Alterar Processo
    [Tags]    Regressivo    Consultar_Alterar    CT02    CT02_05

    Fazer login
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Instrução Normativa"
    Validar Capa Processo
    Preencher dados da capa do processo e salvar
    Clicar em Consultar/Alterar Processo
    Alterar o tipo do processo para "Portaria"
    Coletar número do Processo Criado
    