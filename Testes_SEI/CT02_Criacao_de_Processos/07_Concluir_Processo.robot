*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***

Criar um processo, incluir um documento e concluir o processo
    [Documentation]
    [Tags]    Regressivo    Concluir_Processo    CT02    CT02_07

    Fazer login
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Instrução Normativa"
    Validar Capa Processo
    Preencher dados da capa do processo e salvar
    Clicar em Incluir Documento
    Selecionar tipo de documento "Despacho" para ser incluído
    Preencher dados da capa do documento incluido
    Assinar
    Coletar número do Processo Criado
    Clicar em Concluir Processo
    Confirmar conclusão do processo
    