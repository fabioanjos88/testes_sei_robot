*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***
Criar um processo e excluir
    [Tags]    Regressivo    Excluir_Processo
    
    Fazer login
    Validar Home
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Estatuto"
    Validar Capa Processo
    Preencher dados da capa do processo e salvar
    Validar Página de Tela de Processo
    Coletar número do Processo Criado
    Clicar em Excluir Processo
    Confirmar Exclusão do Processo
