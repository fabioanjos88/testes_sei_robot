*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***
Criar um processo e sobrestar vinculado
    [Tags]    Regressivo    Sobrestar

    Fazer login
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Instrução Normativa"
    Validar Capa Processo
    Preencher dados da capa do processo e salvar
    Coletar número do Processo Criado
    Clicar em Sobrestar
    Preencher sobrestar vinculado


Criar um processo e somente sobrestar
    [Tags]    Regressivo    Sobrestar

    Fazer login
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Instrução Normativa"
    Validar Capa Processo
    Preencher dados da capa do processo e salvar
    Coletar número do Processo Criado
    Clicar em Sobrestar
    Preencher somemte sobrestar