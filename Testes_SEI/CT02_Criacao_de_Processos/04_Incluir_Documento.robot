*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***

Criando um processo e incluindo um documento
    [Tags]    Regressivo    Incluir_Documento    CT02    CT02_01

    Fazer login
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Estatuto"
    Validar Capa Processo
    Preencher dados da capa do processo e salvar
    Clicar em Incluir Documento
    Selecionar tipo de documento "Despacho" para ser incluído
    Preencher dados da capa do documento incluido
    Assinar
    Coletar número do Processo Criado
    