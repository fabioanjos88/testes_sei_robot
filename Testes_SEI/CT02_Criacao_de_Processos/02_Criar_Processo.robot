*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***
    
Criar um Processo
    [Tags]    Regressivo    Criar_Processo    CT02    CT02_02
    
    Fazer login
    Validar Home
    # Sleep    ${TIME_SLEEP}    #PARA VERIFICAR INSTABILIDADE / RETIRAR PARA EXECUÇÃO DE TESTES
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Estatuto"
    Validar Capa Processo
    Preencher dados da capa do processo e salvar
    Validar Página de Tela de Processo
    Coletar número do Processo Criado

