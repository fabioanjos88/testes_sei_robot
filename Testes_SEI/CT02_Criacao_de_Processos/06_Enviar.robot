*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***

Criar um Processo e Enviar
    [Documentation]
    [Tags]    Regressivo    Enviar    CT02    CT02_06

    Fazer login
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Estatuto"
    Preencher dados da capa do processo e salvar
    Clicar em Enviar
    Preencher dados da capa do documento a ser enviado e enviar
    