*** Settings ***
Resource   ../../Resources/main.robot

Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador

*** Test Cases ***

Criar um Processo e adicionando Ciência
    [Documentation]    Atribuição de ciência em um processo ou documento gerado. Uma vez dada ciência, a ação não poderá ser desfeita.
    [Tags]    Regressivo    Ciencia    CT02    CT02_03
 
    Fazer login
    Clicar em Iniciar Processo
    Selecionar processo do tipo "Decreto"
    Preencher dados da capa do processo e salvar
    Clicar em Ciencia
    Validacao Tela Ciência
    Coletar número do Processo Criado
    