*** Settings ***
Resource   ../../Resources/CT00/unidades.robot
Resource   ../../Resources/main.robot
Test Setup       Abrir o navegador (SIP)
Test Teardown    Fechar o navegador
Test Template    Criar unidades


*** Test Cases ***    sigla_unidade    descricao_unidade
Unidade 3	UNI 3	Unidade 3
Unidade 4	UNI 4	Unidade 4
Unidade 5	UNI 5	Unidade 5
Unidade 6	UNI 6	Unidade 6
Unidade 7	UNI 7	Unidade 7


*** Keywords ***
Criar unidades
    [Arguments]    ${sigla_unidade}    ${descricao_unidade}
    Fazer login SIP
    Acessar meu Unidades
    Clicar em Nova
    Selecionar orgao
    Inserir sigla    ${sigla_unidade}
    Inserir descricao    ${descricao_unidade}
    Salvar


